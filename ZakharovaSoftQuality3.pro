#-------------------------------------------------
#
# Project created by QtCreator 2015-11-28T21:37:04
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ZakharovaSoftQuality3
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    mz3class.cpp

HEADERS  += mainwindow.h \
    mz3class.h

FORMS    += mainwindow.ui
