#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QString>
#include <QFile>
#include <QFileDialog>
#include <QMessageBox>
#include <QTextStream>
#include <QTextEdit>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButtonExit_clicked()
{
    this->close();
}

void MainWindow::on_pushButtonMetrics_clicked()
{
    MZ3Class m;
    QString x, w;
    QString words[12];

    x = m.TotalCalculation();
    QStringList list1 = x.split(" ");
    for (int i = 0; i < 12; i++)
    {
        w = list1.at(i);
        words[i] = w;
    }

    ui->lineEditStringsNumber->setText(words[0]);
    ui->lineEditEmptyStringsNumber->setText(words[1]);
    ui->lineEditCommentStringsNumber->setText(words[2]);
    ui->lineEditCodeCommentStringsNumber->setText(words[3]);
    ui->lineEditCommentPers->setText(words[4]);
    ui->lineEditOperatorDict->setText(words[5]);
    ui->lineEditOperandDict->setText(words[6]);
    ui->lineEditOperatorAll->setText(words[7]);
    ui->lineEditOperandAll->setText(words[8]);
    ui->lineEditLength->setText(words[9]);
    ui->lineEditValue->setText(words[10]);
    ui->lineEditCycle->setText(words[11]);
}
