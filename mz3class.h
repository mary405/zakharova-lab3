#ifndef MZ3CLASS_H
#define MZ3CLASS_H

#include <QObject>
#include <QCoreApplication>
#include <QFile>
#include <QTextStream>
#include <QTextCodec>
#include <QStringList>
#include <QDebug>
#include <math.h>

class MZ3Class
{
private:
    QFile *file;
    short StringsNumber;
    short EmptyStringsNumber;
    short CommentStringsNumber;
    short CodeCommentStringsNumber;
    short CommentPerc;

    short OperatorsNumber;
    short OperandsNumber;

    short UniqueOperatorsNumber;
    short UniqueOperandsNumber;
    short SeparatorsNumber;
    short MaxLevel;

    QStringList Operators;
    QStringList UniqueOperators;
    QStringList UniqueOperands;
public:
    MZ3Class();
    QString TotalCalculation();
    void initOperators();
    void calcStringsNumber();
    void calcEmptyStringsNumber();
    void calcCommentStringsNumber();
    void calcCodeCommentStringsNumber();
    void calcCommentPerc();

    void calcOperatorsNumber();
    void calcOperandsNumber();

    void calcSeparatorsNumber();
    void calcMaxLevel();
};

#endif // MZ3CLASS_H
